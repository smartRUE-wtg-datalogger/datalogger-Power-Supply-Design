Power Supply design for sensor modules, core module, and external devices.

Core module will be supplied a dc voltage that could come from a set of external
sources such as dc battery bus, wtg ac rectified, external battery pack, solar 
energy module with battery backup etc, and will form, distribute and condition 
the proper voltage supply to external connected devices.

 In this repository all the power supply related components will be designed and 
 developed.